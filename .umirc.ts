import { defineConfig } from 'umi';

export default defineConfig({
  nodeModulesTransform: {
    type: 'none',
  },
  fastRefresh: {},
});


// routes: [
//   {
//     path: '/auth', 
//     component: '@/layouts/auth',
//     routes: [
//       { path: '/login', component: '@/pages/auth/login' },
//       { component: '@/pages/auth/404' },
//     ]
//   },
//   {
//     path: '/',
//     component: '@/layouts/panel',
//     routes: [
//       { path: '/panel', component: 'panel' },
//       { component: '@/pages/404' },
//     ]
//   },
// ],